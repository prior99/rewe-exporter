import * as React from "react";
import { ConfigStateOptions } from "./config";
import { SetupForm } from "./ui/setup-form";
import { PopupContainer } from "./popup-container";
import "./popup-setup.scss";
import { App, ContextApp } from "./app";

export type PopupSetupCallback = (options: ConfigStateOptions) => void;

export class PopupSetup extends PopupContainer {
    constructor(private app: App) {
        super();
    }

    private listeners: PopupSetupCallback[] = [];

    public addListener(listener: PopupSetupCallback): void {
        this.listeners.push(listener);
    }

    protected render(): JSX.Element {
        return (
            <ContextApp.Provider value={this.app}>
                <SetupForm
                    onSubmit={(options) => {
                        this.listeners.forEach((listener) => listener(options));
                    }}
                />
            </ContextApp.Provider>
        );
    }
}
