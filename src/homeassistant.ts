import { Config, ConfigStateOptions, ConfigStatus } from "./config";
import { UnitAndQuantity } from "./units";

export interface ProductLine {
    readonly id: string;
    readonly search: string;
}

export interface RawShoppingListItem {
    readonly name: string;
    readonly id: string;
    readonly complete: boolean;
}

export interface ShoppingListItem extends UnitAndQuantity {
    readonly name: string;
    readonly id: string;
    complete: boolean;
    readonly originalName: string;
}

export class Homeassistant {
    constructor(private config: Config) {}

    private get configOptions(): ConfigStateOptions {
        if (this.config.state.status !== ConfigStatus.LOADED) {
            throw new Error("Config not initialized.");
        }
        return this.config.state;
    }

    public async fetchShoppingList(): Promise<readonly RawShoppingListItem[]> {
        const request = await fetch(`${this.configOptions.baseUrl}/api/shopping_list`, {
            headers: {
                authorization: `Bearer ${this.configOptions.accessToken}`,
            },
        });
        return await request.json();
    }

    public async markShoppingListItemComplete(name: string): Promise<void> {
        const request = await fetch(`${this.configOptions.baseUrl}/api/services/shopping_list/complete_item`, {
            method: "POST",
            headers: {
                authorization: `Bearer ${this.configOptions.accessToken}`,
            },
            body: JSON.stringify({
                name,
            }),
        });
        await request.text();
    }

    public async fetchDatabase(): Promise<readonly ProductLine[]> {
        const request = await fetch(`${this.configOptions.baseUrl}/local/rewe`, {
            headers: {
                authorization: `Bearer ${this.configOptions.accessToken}`,
            },
        });
        const lines = await request.text();
        return lines.split("\n").map((line) => {
            const idEnd = line.indexOf(":");
            const id = line.substring(0, idEnd);
            const search = line.substring(idEnd + 1, line.length);
            return {
                id: id.trim(),
                search: search.trim(),
            };
        });
    }

    public async addDatabaseLine(id: string, search: string): Promise<void> {
        const request = await fetch(`${this.configOptions.baseUrl}/api/services/script/add_rewe_database_entry`, {
            method: "POST",
            headers: {
                authorization: `Bearer ${this.configOptions.accessToken}`,
            },
            body: JSON.stringify({
                id,
                search,
            }),
        });
        await request.text();
    }
}
