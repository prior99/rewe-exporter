import { action, computed, makeObservable, observable } from "mobx";
import { Homeassistant } from "./homeassistant";

export class Database {
    @observable private products = new Map<string, string>();

    constructor(private homeassistant: Homeassistant) {
        makeObservable(this);
    }

    @action.bound private registerProduct(search: string, reweId: string): void {
        this.products.set(search.trim(), reweId);
    }

    @computed public get count(): number {
        return this.products.size;
    }

    public getId(search: string): string | undefined {
        return this.products.get(search.trim());
    }

    public has(search: string): boolean {
        return this.products.has(search.trim());
    }

    @action.bound public async load(): Promise<void> {
        const lines = await this.homeassistant.fetchDatabase();
        lines.forEach((line) => this.registerProduct(line.search, line.id));
        console.info(`Database initialized with ${this.count} associations.`);
    }

    @action.bound public async addProduct(search: string, reweId: string): Promise<void> {
        await this.homeassistant.addDatabaseLine(reweId, search);
        this.registerProduct(search, reweId);
    }

    public getSearches(reweId: string): string[] {
        return [...this.products.entries()].filter(([_, value]) => value === reweId).map(([key]) => key);
    }
}
