import * as React from "react";
import * as ReactDOM from "react-dom";
import { App, ContextApp } from "./app";
import { LinkButton } from "./ui/link-button";

export class Linker {
    private links: HTMLElement[] = [];

    constructor(private app: App) {}

    public async createLinks(): Promise<void> {
        this.links.forEach((link) => link.parentNode.removeChild(link));

        const productIds = await this.app.reweAdapter.getAllProductIds();
        console.log(`Injecting buttons into ${productIds.length} products.`, productIds);

        this.links = await Promise.all(
            productIds.map(async (id) => {
                const root = await this.app.reweAdapter.injectButtonIntoProduct(id);
                ReactDOM.render(
                    <ContextApp.Provider value={this.app}>
                        <LinkButton reweId={id} />
                    </ContextApp.Provider>,
                    root,
                );
                return root;
            }),
        );
    }
}
