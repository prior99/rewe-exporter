import { action, computed, makeObservable, observable } from "mobx";
import { Homeassistant, ShoppingListItem } from "./homeassistant";
import { convertUnit, Unit } from "./units";

export class ShoppingList {
    @observable private list: readonly ShoppingListItem[] = [];

    constructor(private homeassistant: Homeassistant) {
        makeObservable(this);
    }

    @action.bound public async load(): Promise<void> {
        const rawList = await this.homeassistant.fetchShoppingList();
        this.list = rawList.map((item) => {
            const regexResult = item.name.match(/^(\d+\.?,?\d*)\s*?(g|x|l|kg|ml)?\s+(.*?)$/);
            if (!regexResult) {
                return {
                    ...item,
                    unit: Unit.PIECE,
                    quantity: 1,
                    factor: 1,
                    originalName: item.name
                };
            }
            const [_, quantity, unit, name] = regexResult;
            return {
                ...item,
                ...convertUnit(unit),
                quantity: Number(quantity.replace(",", ".")),
                name,
                originalName: item.name,
            };
        });

        console.info(
            `Shopping list loaded. ${this.allItems.length} items (${this.incompleteItems.length} incomplete).`,
        );
    }

    @computed public get allItems(): ShoppingListItem[] {
        return [...this.list];
    }

    @computed public get incompleteItems(): ShoppingListItem[] {
        return [...this.list].filter(({ complete }) => !complete);
    }

    @action.bound public async markItemAsComplete(name: string): Promise<void> {
        await this.homeassistant.markShoppingListItemComplete(name);
        const item = this.list.find((entry) => entry.originalName === name);
        if (item) {
            item.complete = true;
        }
    }

    public filterByHomeassistantIds(...ids: string[]): ShoppingListItem[] {
        return this.allItems.filter(({ id }) => ids.includes(id));
    }
}
