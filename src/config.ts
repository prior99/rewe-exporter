import { PopupSetup } from "./popup-setup";

const identifier = "rewe-config";

export enum ConfigStatus {
    LOADED = "loaded",
    LOADING = "loading",
    ERROR = "error",
    UNCONFIGURED = "unconfigured",
    INITIAL = "initial",
}

export interface ConfigStateOptions {
    readonly accessToken: string;
    readonly baseUrl: string;
}

export type ConfigState =
    | {
          readonly status: ConfigStatus.INITIAL;
      }
    | {
          readonly status: ConfigStatus.ERROR;
          readonly reason: string;
          readonly error?: Error;
      }
    | ({
          readonly status: ConfigStatus.LOADED;
      } & ConfigStateOptions)
    | {
          readonly status: ConfigStatus.UNCONFIGURED;
      }
    | {
          readonly status: ConfigStatus.LOADING;
      };

export class Config {
    constructor(private popupSetup: PopupSetup) {}

    private _state: ConfigState = { status: ConfigStatus.INITIAL };

    private validateState(state: unknown): state is ConfigStateOptions {
        return state !== null && typeof state === "object" && ["accessToken", "baseUrl"].every((key) => key in state);
    }

    private async loadStateFromGM(): Promise<ConfigState> {
        if (!(await GM.listValues()).includes(identifier)) {
            return { status: ConfigStatus.UNCONFIGURED };
        }
        let jsonState: string;
        try {
            jsonState = await GM.getValue(identifier);
        } catch (error) {
            return { status: ConfigStatus.ERROR, reason: "Unable to load config values from GM.", error };
        }
        let parsedState: unknown;
        try {
            parsedState = JSON.parse(jsonState);
        } catch (error) {
            return { status: ConfigStatus.ERROR, reason: "Unable to deserialize values.", error };
        }
        if (!this.validateState(parsedState)) {
            return {
                status: ConfigStatus.ERROR,
                reason: "State invalid.",
            };
        }
        return {
            status: ConfigStatus.LOADED,
            ...parsedState,
        };
    }

    public async load(): Promise<void> {
        this._state = {
            status: ConfigStatus.LOADING,
        };
        this._state = await this.loadStateFromGM();

        console.info(`Status after loading config: ${this.state.status}`);
        if (this.state.status !== ConfigStatus.LOADED) {
            const options = await new Promise<ConfigStateOptions>((resolve) => {
                this.popupSetup.addListener(resolve);
                this.popupSetup.open();
            });
            this.popupSetup.hide();
            await this.save(options);
        }

        if (this.state.status !== ConfigStatus.LOADED) {
            console.error("Config failed to be setup.");
            await this.clear();
            return;
        }
    }

    public get state(): ConfigState {
        return this._state;
    }

    public async save(options: ConfigStateOptions): Promise<void> {
        this._state = {
            status: ConfigStatus.LOADED,
            ...options,
        };
        await GM.setValue(identifier, JSON.stringify(options));
    }

    public async clear(): Promise<void> {
        await GM.deleteValue(identifier);
    }
}
