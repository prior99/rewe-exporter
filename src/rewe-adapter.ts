import { convertUnit, getCount, Unit, UnitAndQuantity } from "./units";

const searchFieldSelector = "input[type=search][name=search]";
const increaseQuantityButtonSelector = "ul#products-list li button.bs_amount-plus";
const basketValueSelector = "form.bsButtonLinkForm span.basket-link_value--price";

const nativeInputValueSetter = Object.getOwnPropertyDescriptor(window.HTMLInputElement.prototype, "value").set;

export class ReweAdapter {
    private async waitForChange<T>(
        condition: () => boolean,
        returnValue: () => T,
        message = "Timed out waiting for condition.",
        timeout = 2000,
    ): Promise<T> {
        return new Promise((resolve, reject) => {
            const startTime = Date.now();
            const fn = (): void => {
                const result = condition();
                if (result) {
                    resolve(returnValue());
                    return;
                } else if (Date.now() - startTime > timeout) {
                    reject(new Error(message));
                    return;
                } else {
                    setTimeout(fn, 100);
                }
            };
            fn();
        });
    }

    private async waitForSelectorAll<T extends HTMLElement>(
        selector: string,
        minCount = 1,
        timeout = 2000,
    ): Promise<T[]> {
        return this.waitForChange<T[]>(
            () => document.querySelectorAll(selector).length >= minCount,
            () => [...document.querySelectorAll(selector)] as T[],
            `Timed out waiting for selector: ${selector}`,
            timeout,
        );
    }

    private async waitForSelector<T extends HTMLElement>(selector: string, timeout = 2000): Promise<T> {
        return this.waitForChange<T>(
            () => document.querySelector(selector) !== null,
            () => document.querySelector(selector),
            `Timed out waiting for selector: ${selector}`,
            timeout,
        );
    }

    private async waitForInnerHTMLToChange(selector: string, timeout = 2000): Promise<void> {
        const initialValue = document.querySelector(selector).innerHTML;
        return this.waitForChange<void>(
            () => document.querySelector(selector).innerHTML !== initialValue,
            () => undefined,
            `Timed out waiting for selector to change: ${selector}`,
            timeout,
        );
    }

    public async injectButtonIntoProduct(id: string): Promise<HTMLElement> {
        const container = await this.waitForSelector(`main [data-productid="${id}"] .bs_add2cart_container`);
        console.log(container);
        const root = document.createElement("div");
        container.insertBefore(root, container.firstChild);
        return root;
    }

    public async getAllProductIds(): Promise<string[]> {
        const links = await this.waitForSelectorAll<HTMLAnchorElement>(".search-service-product meso-data");
        return links.map((elem) => elem.getAttribute("data-productid")).filter((id) => id !== undefined);
    }

    private searchField(): Promise<HTMLInputElement> {
        return this.waitForSelector(searchFieldSelector);
    }

    private increaseQuantityButton(): Promise<HTMLInputElement> {
        return this.waitForSelector(increaseQuantityButtonSelector);
    }

    private async enterSearch(search: string): Promise<void> {
        const searchField = await this.searchField();
        nativeInputValueSetter.call(searchField, search);
        const event = new Event("input", { bubbles: true });
        searchField.dispatchEvent(event);
    }

    private async increaseQuantityFirstSearchResult(): Promise<void> {
        const increaseQuantityButton = await this.increaseQuantityButton();
        increaseQuantityButton.click();
    }

    private async waitForBasketToUpdate(): Promise<void> {
        await this.waitForInnerHTMLToChange(basketValueSelector);
    }

    private async waitForProduct(id: string): Promise<HTMLAnchorElement> {
        return await this.waitForSelector(`a[href$="/${id}"]`);
    }

    private async findUnitAndQuantity(id: string): Promise<UnitAndQuantity> {
        const element = (await this.waitForProduct(id)).parentElement.parentElement.children[1].lastElementChild;
        const text = element.innerHTML;
        const regexResult = text.match(/(\d+\.?,?\d*)\s?(g|x|ml|l|Stück)/);
        if (!regexResult) {
            return { quantity: 1, unit: Unit.PIECE, factor: 1 };
        }
        const [_, quantityRaw, unitRaw] = regexResult;
        const quantity = Number(quantityRaw.replace(",", "."));
        const { unit, factor } = convertUnit(unitRaw);
        if (isNaN(quantity) || !unit) {
            return { quantity: 1, unit: Unit.PIECE, factor: 1 };
        }
        return { quantity, unit, factor };
    }

    public async addToBasket(id: string, desired: UnitAndQuantity): Promise<boolean> {
        const productPromise = this.waitForProduct(id);
        await this.enterSearch(id);
        await productPromise;

        await new Promise((resolve) => setTimeout(resolve, 400));

        const actual = await this.findUnitAndQuantity(id);
        console.info(`Found actual product quantity ${actual.quantity} (factor: ${actual.factor}) ${actual.unit}.`);
        console.info(`Got desired quantity ${desired.quantity} (factor: ${desired.factor}) ${desired.unit}.`);

        if (desired.unit !== Unit.PIECE && desired.unit !== actual.unit) {
            console.warn(`Unit ${actual.unit} doesn't match desired unit ${desired.unit}.`);
            return false;
        }

        const count = getCount(actual, desired);
        console.info(`Decided to buy a quantity of ${count} products.`);

        for (let i = 0; i < count; ++i) {
            const basketPromise = this.waitForBasketToUpdate();
            await this.increaseQuantityFirstSearchResult();
            await basketPromise;
            await new Promise((resolve) => setTimeout(resolve, 100));
        }

        return true;
    }
}
