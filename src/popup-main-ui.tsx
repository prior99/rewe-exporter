import * as React from "react";
import { App, ContextApp } from "./app";
import { PopupContainer } from "./popup-container";
import { MainUi } from "./ui/main-ui";

export class PopupMainUi extends PopupContainer {
    constructor(private app: App) {
        super();
    }

    protected render(): JSX.Element {
        return (
            <ContextApp.Provider value={this.app}>
                <MainUi />
            </ContextApp.Provider>
        );
    }
}
