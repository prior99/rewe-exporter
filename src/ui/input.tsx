import * as React from "react";
import classNames from "classnames";
import { v4 as uuid } from "uuid";
import "./input.scss";

export interface InputProps
    extends React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> {
    readonly label?: string;
}

export function Input({ label, className, ...props }: InputProps): JSX.Element {
    const [id] = React.useState(() => uuid());

    return (
        <div className={classNames(className, "Input")}>
            {label && (
                <label className="Input__label" htmlFor={id}>
                    {label}
                </label>
            )}
            <input className="Input__input" id={id} {...props} />
        </div>
    );
}
