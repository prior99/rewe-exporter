import classNames from "classnames";
import { observer } from "mobx-react";
import * as React from "react";
import { ContextApp } from "../app";
import { ShoppingListItem } from "../homeassistant";
import { formatUnit } from "../units";
import "./product-item.scss";

export interface ProductItemProps {
    readonly item: ShoppingListItem;
    readonly onChange: (enabled: boolean) => void;
    readonly enabled: boolean;
    readonly unmatched: boolean;
}

export const ProductItem = observer(({ item, onChange, enabled, unmatched }: ProductItemProps): JSX.Element => {
    const { database } = React.useContext(ContextApp);
    const handleChange = (): void => onChange(!enabled);

    return (
        <li className="ProductItem" onClick={handleChange}>
            <span className={classNames("ProductItem__name", { "ProductItem__name--disabled": !enabled })}>
                {item.name}
            </span>
            <span className={classNames("ProductItem__quantity")}>{item.quantity}</span>
            <span className={classNames("ProductItem__unit")}>{formatUnit(item).toUpperCase()}</span>
            <span className={classNames("ProductItem__id", { "ProductItem__id--unmatched": unmatched })}>
                {unmatched ? "???" : database.getId(item.name)}
            </span>
        </li>
    );
});
