import { observer } from "mobx-react";
import * as React from "react";
import { ContextApp } from "../app";
import "./link-button-tooltip.scss";

export interface LinkButtonTooltipProps {
    readonly reweId: string;
}

export const LinkButtonTooltip = observer(({ reweId }: LinkButtonTooltipProps): JSX.Element => {
    const [search, setSearch] = React.useState("");

    const { database } = React.useContext(ContextApp);
    const searches = database.getSearches(reweId);
    const disabled = search.length < 0 || database.has(search);

    const handleSubmit = async (evt: React.SyntheticEvent<HTMLFormElement>): Promise<void> => {
        evt.preventDefault();
        if (disabled) {
            return;
        }
        await database.addProduct(search, reweId);
        setSearch("");
    };

    return (
        <div className="LinkButtonTooltip">
            <div className="LinkButtonTooltip__searches">{searches.join(", ")}</div>
            <form className="LinkButtonTooltip__add" onSubmit={handleSubmit}>
                <input
                    className="LinkButtonTooltip__inputText"
                    type="text"
                    onChange={(evt) => setSearch(evt.target.value)}
                    value={search}
                />
                <button disabled={disabled} className="LinkButtonTooltip__inputButton" type="submit">
                    OK
                </button>
            </form>
        </div>
    );
});
