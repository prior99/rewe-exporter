import { observer } from "mobx-react";
import * as React from "react";
import { LinkButtonTooltip } from "./link-button-tooltip";
import "./link-button.scss";

export interface LinkButtonProps {
    readonly reweId: string;
}

export const LinkButton = observer(({ reweId }: LinkButtonProps): JSX.Element => {
    const [expanded, setExpanded] = React.useState(false);

    return (
        <div className="LinkButton">
            {expanded && <LinkButtonTooltip reweId={reweId} />}
            <button className="LinkButton__button" onClick={() => setExpanded(!expanded)}>
                LN
            </button>
        </div>
    );
});
