import classNames from "classnames";
import { observer } from "mobx-react";
import * as React from "react";
import { ContextApp } from "../app";
import { ShoppingListItem } from "../homeassistant";
import { ProductItem } from "./product-item";
import "./button.scss";
import "./main-ui.scss";

export interface MainUiProps {}

export const MainUi = observer((_: MainUiProps): JSX.Element => {
    const app = React.useContext(ContextApp);

    const [enabledIds, setEnabledIds] = React.useState(app.shoppingList.incompleteItems.map((item) => item.id));

    const handleProductEnabledChange = (item: ShoppingListItem) => (enabled: boolean) => {
        if (enabled) {
            setEnabledIds([...enabledIds, item.id]);
        } else {
            setEnabledIds(enabledIds.filter((id) => id !== item.id));
        }
    };

    return (
        <div className="MainUi">
            <ul className="MainUi__productItemList">
                {app.shoppingList.incompleteItems.map((item) => (
                    <ProductItem
                        unmatched={!app.database.has(item.name)}
                        item={item}
                        enabled={enabledIds.includes(item.id)}
                        onChange={handleProductEnabledChange(item)}
                    />
                ))}
            </ul>
            <div className="MainUi__buttons">
                <button
                    className={classNames("Button", "Button--shoppingCart")}
                    onClick={() => app.processShoppingList(enabledIds)}
                >
                    Add To Shoppingcart
                </button>
                <button className={classNames("Button", "Button--link")} onClick={() => app.linker.createLinks()}>
                    Create Link Buttons
                </button>
                <button className={classNames("Button", "Button--reset")} onClick={() => app.config.clear()}>
                    Reset Config
                </button>
            </div>
        </div>
    );
});
