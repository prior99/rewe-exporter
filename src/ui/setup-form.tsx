import { observer } from "mobx-react";
import * as React from "react";
import { ConfigStateOptions } from "../config";
import { Input } from "./input";

export interface SetupFormProps {
    readonly onSubmit: (options: ConfigStateOptions) => void;
}

export const SetupForm = observer(({ onSubmit }: SetupFormProps): JSX.Element => {
    const [baseUrl, setBaseUrl] = React.useState("");
    const [accessToken, setAccessToken] = React.useState("");

    const handleSubmit = (evt: React.SyntheticEvent<HTMLFormElement>): void => {
        evt.preventDefault();
        onSubmit({ baseUrl, accessToken });
    };

    return (
        <form className="SetupUi" onSubmit={handleSubmit}>
            <Input
                label="Access Token"
                type="text"
                value={accessToken}
                onChange={(evt) => setAccessToken(evt.target.value)}
            />

            <Input
                label="Base URL"
                className="SetupUi__input"
                type="text"
                value={baseUrl}
                onChange={(evt) => setBaseUrl(evt.target.value)}
            />

            <Input type="submit" value="Okay" />
        </form>
    );
});
