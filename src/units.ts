export enum Unit {
    PIECE = "piece",
    GRAM = "gram",
    LITER = "liter",
}

export interface UnitAndFactor {
    readonly factor: number;
    readonly unit: Unit;
}

export interface UnitAndQuantity extends UnitAndFactor {
    readonly quantity: number;
}

export function convertUnit(unitName: string | undefined | null): UnitAndFactor | undefined {
    switch (unitName) {
        case "g":
            return { unit: Unit.GRAM, factor: 1 };
        case "kg":
            return { unit: Unit.GRAM, factor: 1000 };
        case "l":
            return { unit: Unit.LITER, factor: 1 };
        case "ml":
            return { unit: Unit.LITER, factor: 0.001 };
        case "":
        case null:
        case undefined:
        case "x":
        case "Stück":
            return { unit: Unit.PIECE, factor: 1 };
        default:
            return undefined;
    }
}

export function getCount(actual: UnitAndQuantity, desired: UnitAndQuantity): number {
    if (desired.unit === Unit.PIECE && actual.unit !== desired.unit) {
        return desired.quantity;
    }
    return Math.ceil((desired.quantity * desired.factor) / (actual.quantity * actual.factor));
}

export function formatUnitOnly({ unit }: UnitAndFactor): string {
    switch (unit) {
        case Unit.GRAM:
            return "g";
        case Unit.PIECE:
            return "x";
        case Unit.LITER:
            return "l";
    }
}

export function formatUnitPrefix({ factor }: UnitAndFactor): string {
    switch (factor) {
        case 1000:
            return "k";
        case 0.001:
            return "m";
        default:
        case 1:
            return "";
    }
}

export function formatUnit(unit: UnitAndFactor): string {
    return `${formatUnitPrefix(unit)}${formatUnitOnly(unit)}`;
}