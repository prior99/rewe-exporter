import * as React from "react";
import { Config } from "./config";
import { ShoppingList } from "./shoppingList";
import { Database } from "./database";
import { Homeassistant } from "./homeassistant";
import { ReweAdapter } from "./rewe-adapter";
import { PopupSetup } from "./popup-setup";
import { PopupMainUi } from "./popup-main-ui";
import { Linker } from "./linker";

export class App {
    public popupSetup = new PopupSetup(this);
    public config = new Config(this.popupSetup);
    public homeassistant = new Homeassistant(this.config);
    public shoppingList = new ShoppingList(this.homeassistant);
    public database = new Database(this.homeassistant);
    public reweAdapter = new ReweAdapter();
    public linker = new Linker(this);
    public popupMain = new PopupMainUi(this);

    public async load(): Promise<void> {
        await this.config.load();
        await this.database.load();
        await this.shoppingList.load();
        await this.linker.createLinks();
    }

    public async processShoppingList(enabledHomeassistantIds: string[]): Promise<void> {
        const items = this.shoppingList
            .filterByHomeassistantIds(...enabledHomeassistantIds)
            .filter((item) => !item.complete);
        try {
            for (const { name, originalName, ...unitAndQuantity } of items) {
                const id = this.database.getId(name);
                console.groupCollapsed(`Adding to basket: ${name} (${id})`);
                try {
                    if (!id) {
                        console.warn(`Unmatched product: ${name}`);
                        continue;
                    }
                    const result = await this.reweAdapter.addToBasket(id, unitAndQuantity);
                    if (result) {
                        console.info("Added.");
                        await this.shoppingList.markItemAsComplete(originalName);
                        console.info("Marked as completed.");
                    } else {
                        console.warn("Failed to add.");
                    }
                } finally {
                    console.groupEnd();
                }
            }
        } catch (err) {
            console.error("Failed to communicate with homeassistant.");
            throw err;
        }
    }
}

export const ContextApp = React.createContext<App | undefined>(undefined);
