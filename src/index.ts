import { App } from "./app";

async function main(): Promise<void> {
    const app = new App();

    await app.load();

    console.info("Opening main ui.");
    app.popupMain.open();
}

main();
