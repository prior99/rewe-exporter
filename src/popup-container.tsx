import * as React from "react";
import * as ReactDOM from "react-dom";
import "./popup-container.scss";

export abstract class PopupContainer {
    private root?: HTMLElement;

    protected abstract render(): JSX.Element;

    public open(): void {
        this.root = document.createElement("section");
        document.body.appendChild(this.root);
        ReactDOM.render(<section className="PopupContainer">{this.render()}</section>, this.root);
    }

    public hide(): void {
        document.body.removeChild(this.root);
    }
}
