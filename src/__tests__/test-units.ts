import { getCount, Unit } from "../units";

describe("getCount", () => {
    it.each([
        {
            actual: { unit: Unit.PIECE, factor: 1, quantity: 2 },
            desired: { unit: Unit.PIECE, factor: 1, quantity: 3 },
            expected: 2,
        },
        {
            actual: { unit: Unit.GRAM, factor: 1, quantity: 300 },
            desired: { unit: Unit.GRAM, factor: 1000, quantity: 2 },
            expected: 7,
        },
        {
            actual: { unit: Unit.LITER, factor: 1, quantity: 250 },
            desired: { unit: Unit.LITER, factor: 1000, quantity: 1 },
            expected: 4,
        },
    ])("$actual, $desired, $expected", ({ actual, desired, expected }) => {
        expect(getCount(actual, desired)).toBe(expected);
    });
});
