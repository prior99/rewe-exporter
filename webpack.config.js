const path = require("path");
const { TampermonkeyWebpackPlugin } = require("tampermonkey-webpack-plugin");

module.exports = {
    entry: {
        "script.user": path.join(__dirname, "src", "index.ts"),
    },
    output: {
        path: path.join(__dirname, "dist"),
    },
    target: "browserslist:last 2 chrome versions",
    resolve: {
        extensions: [".js", ".jsx", ".ts", ".tsx"],
    },
    module: {
        rules: [
            {
                test: /\.tsx?/,
                loader: "ts-loader",
                options: {
                    configFile: path.join(__dirname, "tsconfig.json"),
                },
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                    { loader: "sass-loader" },
                ],
            },

        ],
    },
    externals: {
        "react": "React",
        "react-dom": "ReactDOM"
    },
    devtool: "source-map",
    plugins: [
        new TampermonkeyWebpackPlugin({
            minAlignSpace: 4,
            header: {
                author: "Frederick Gnodtke",
                name: ["rewe-exporter"],
                description: ["Export homeassistant shopping list to REWE"],
                namespace: "https://shop.rewe.de/",
                version: "0.1.3",
                grant: ["GM.setValue", "GM.getValue", "GM.listValues", "GM.deleteValue"],
                include: ["https://*.rewe.de/*"],
                updateURL: "https://gitlab.com/prior99/rewe-exporter/-/jobs/artifacts/master/raw/dist/script.user.js?job=build",
                downloadURL: "https://gitlab.com/prior99/rewe-exporter/-/jobs/artifacts/master/raw/dist/script.user.js?job=build"
            },
        })
    ]
};
